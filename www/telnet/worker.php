<?php

/**
 *  Worker for getting ticks and put it in DB
 */

// autoloader
spl_autoload_register(function ($class) {
    include_once '../helpers/' . $class . '.php';
});

/** @var $client TelnetClient */
$client = new TelnetClient(App::$config['telnet']['ip'], App::$config['telnet']['port']);

$client->socketCreate();
$client->connectTelnet();


/** @todo Move to PDO only */
/** @var $connect Db */
$connect = new Db(App::$config['bufferDB']['host'], App::$config['bufferDB']['dbName'], App::$config['bufferDB']['user'], App::$config['bufferDB']['pass']);
/** @var $db PDO */
$db = $connect->connect();


// Endless loop
do {
    $tick = $client->getTicks();
    $tickArray = explode(";", trim($tick));
    foreach ($tickArray as $key => $field)
        $tickArray[$key] = substr($field, 2); // Remove useless data aka S= and etc.

    $tickArray[0] = Symbol::convertToId($tickArray[0]);

    if ($tickArray[0] === false)
        continue;

    $query = $db->prepare(
        'INSERT INTO ' . App::$config['buffer']['tableName'] . ' (`symbol_id`, `bid`, `ask`, `time`) values (:symbol_id, :bid, :ask, :time)'
    );
    $query->execute(
        array(
            ":symbol_id" => $tickArray[0],
            ":time" => $tickArray[1],
            ":bid" => $tickArray[2],
            ":ask" => $tickArray[3],
        )
    );

} while (true);