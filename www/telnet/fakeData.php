<?php

// autoloader
spl_autoload_register(function ($class) {
    include_once '../helpers/' . $class . '.php';
});

$connect = new Db(App::$config['bufferDB']['host'], App::$config['bufferDB']['dbName'], App::$config['bufferDB']['user'], App::$config['bufferDB']['pass']);
$connect->connect();
$db = $connect->db;

$price = 678;
do{

    $bid = $price;

    if (rand(0, 1))
        --$price;
    else
        ++$price;

    $ask = $price;

    $tickArray = array(
        Symbol::GOLD,
        date('H:i:s'),
        $bid,
        $ask
    );
    $query = $db->prepare(
        "INSERT INTO buffer (`symbol_id`, `bid`, `ask`, `time`) values (:symbol_id, :bid, :ask, :time)"
    );
    $query->execute(
        array(
            ":symbol_id" =>$tickArray[0],
            ":time" =>$tickArray[1],
            ":bid" =>$tickArray[2],
            ":ask" =>$tickArray[3],
        )
    );
    usleep(500000);
}while(true);
