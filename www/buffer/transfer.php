<?php
/**
 *  Transfer data from buffer to history tables
 */

error_reporting(E_ALL);
ini_set('display_errors', 'On');

// autoloader
spl_autoload_register(function ($class) {
    include_once '../helpers/' . $class . '.php';
});

/** @var Db $connect */
$connect = new Db(App::$config['bufferDB']['host'], App::$config['bufferDB']['dbName'], App::$config['bufferDB']['user'], App::$config['bufferDB']['pass']);
$transfer = new Transfer();
$transfer->db = $connect->connect();


// Endless loop
do {
    $transfer->getBufferRow();

    foreach ($transfer->getPeriods() as $period) {
        if ($transfer->getTimeBar($period)) {
            $transfer->updateTimeBar($period);
        } else {
            $transfer->createTimeBar($period);
        }
    }

    $transfer->incrementSequence();

} while (true);