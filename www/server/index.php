<?php
// composer loader
include_once '../../vendor/autoload.php';
// autoloader
spl_autoload_register(function ($class) {
    include_once '../helpers/' . $class . '.php';
});

/** @var Db $connect */
$connect = new Db(App::$config['bufferDB']['host'], App::$config['bufferDB']['dbName'], App::$config['bufferDB']['user'], App::$config['bufferDB']['pass']);
/** @var $server Server */
$server = new Server();
$server->db = $connect->connect();
$server->init();

/*
$params = array(
    'symbol' => 'EUR/USD',
    'period' => '1m',
    'since' => 0
);

var_dump( $server->getHistory($params) );
*/