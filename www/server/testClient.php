<?php
// composer loader
include_once '../../vendor/autoload.php';
// autoloader
spl_autoload_register(function ($class) {
    include_once '../helpers/' . $class . '.php';
});

$params = array(
    'symbol' => 'EUR/USD',
    'period' => '1m',
    'since' => 0
);

$request = Tivoka\Client::request('history.json', $params);

$target = 'http://example.com/server/index.php';
Tivoka\Client::connect($target)->send($request);


if($request->isError()) var_dump($request->errorMessage);
else var_dump($request->result);