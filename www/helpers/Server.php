<?php
// composer loader
include_once '../../vendor/autoload.php';
// autoloader
spl_autoload_register(function ($class) {
    include_once '../helpers/' . $class . '.php';
});

class Server {
    /** @var  array */
    public $methods;

    /** @var $db PDO */
    private $db;

    /**
     * Magic getter
     *
     * @param $name
     * @return mixed
     * @throws Exception
     */
    public function __get($name)
    {
        if (method_exists($this, $method = ('get' . ucfirst($name))))
            return $this->$method();
        else
            throw new Exception('Can\'t get property ' . $name);
    }

    /**
     * Magic setter
     *
     * @param $name
     * @param $value
     * @return mixed
     * @throws Exception
     */
    public function __set($name, $value)
    {
        if (method_exists($this, $method = ('set' . ucfirst($name))))
            return $this->$method($value);
        else
            throw new Exception('Can\'t set property ' . $name);
    }

    /**
     * @return PDO
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @param PDO $db
     * @throws Exception
     */
    public function setDb(PDO $db)
    {
        if ($db instanceof PDO)
            $this->db = $db;
        else
            throw new Exception("Try set not PDO");
    }

    /**
     * @internal param array $params
     */
    public function init(){
        $controller = $this;
        $this->methods = array(
            'history.json' => function ($params) use ($controller) {
                    return $controller->getHistory($params);
                },
        );
        Tivoka\Server::provide($this->methods)->dispatch(file_get_contents('php://input'));
    }

    /**
     * Get history from DB
     * @param array $params
     * <pre>
     *        $params['symbol']
     *        $params['period']
     *        $params['since']
     * </pre>
     * @throws Exception
     * @return array|bool
     */
    public function getHistory(array $params)
    {

        /** @var $symbolId integer */
        if(!($symbolId = Symbol::convertToId($params['symbol'])))
            throw new Exception("Symbol ".$params['symbol']." not found");

        /** @var $table string */
        $table = Symbol::getTable($symbolId, $params['period']);

        if($table===false)
            throw new Exception("Table ".$table." not found");

        /** @var $query PDOStatement */
        $query = $this->db->prepare("SELECT * FROM " . $table . " WHERE datetime >= :since");
        $query->execute(array(
            ":since" => $params['since']
        ));

        if ($query->rowCount() == 0)
            return false;
        else
            return $query->fetchAll(PDO::FETCH_ASSOC);
    }
}
