<?php

class Symbol {


    const GOLD      = 1;
    const BRENT     = 2;
    const LIGHT     = 3;
    const EUR_USD   = 4;
    const GPB_USD   = 5;


    const MINUTE    = "1m";
    const HOUR      = "1h";
    const DAY       = "1d";

    /**
     * @param $id integer
     * @param $period string
     * <pre>
     *     Values 1m, 1h, 1d
     * </pre>
     *
     * @throws Exception
     * @return string
     */
    static public  function getTable($id, $period)
    {
        switch ($id)
        {
            case self::GOLD :
                $name = "gold".$period;
                break;
            case self::BRENT :
                $name = "brent".$period;
                break;
            case self::LIGHT :
                $name = "light".$period;
                break;
            case self::EUR_USD :
                $name = "eurusd".$period;
                break;
            case self::GPB_USD :
                $name = "gpbusd".$period;
                break;
        }

        if(empty($name))
            throw new Exception("History table not found.");

        return $name;
    }

    /**
     * Convert Symbol name to ID
     *
     * @param $name
     * @return bool|int
     */
    static public function convertToId($name)
    {

        switch ($name)
        {
            case "GOLD" :
                $id = Symbol::GOLD;
                break;
            case "BRENT" :
                $id = Symbol::BRENT;
                break;
            case "LIGHT" :
                $id = Symbol::LIGHT;
                break;
            case "EUR/USD" :
                $id = Symbol::EUR_USD;
                break;
            case "GBP/USD" :
                $id = Symbol::GPB_USD;
                break;
            default:
                $id = null;
        }

        if(empty($id))
            return false;

        return $id;
    }
}