<?php

/**
 * Class App
 *
 */
class App
{
    /** @static $config array */
    static public $config = array(
        'telnet' => array(
            'ip' => '178.62.145.164',
            'port' => 10000,
        ),
        'bufferDB' => array(
            'host' => 'localhost',
            'dbName' => 'example',
            'user' => 'exampleUser',
            'pass' => 'examplePass',
        ),
        'buffer' => array(
            'tableName' => 'buffer',
            'sequenceLabel' => 'buffer',
        ),
        'sequence' => array(
            'tableName' => 'sequence'
        ),
        'transfer' => array(
            'sleep' => 1000000
        ),

    );
}