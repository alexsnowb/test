<?php

/**
 * Class Db
 * Helper class for work with DB
 */
class Db {

    /** @var $db PDO */
    public  $db;

    private $host;
    private $dbName;
    private $user;
    private $pass;

    /**
     * @param $host
     * @param $dbName
     * @param $user
     * @param $pass
     */
    public function __construct($host, $dbName, $user, $pass)
    {
        $this->host = $host;
        $this->dbName = $dbName;
        $this->user = $user;
        $this->pass = $pass;
    }

    /**
     * DB connect
     *
     * @return PDO
     */
    public function connect()
    {
        try {
            // MySQL
            $this->db = new PDO("mysql:host=$this->host;dbname=$this->dbName", $this->user, $this->pass);
        }
        catch(PDOException $e) {
            die( $e->getMessage() );
        }
        return $this->db;
    }

} 