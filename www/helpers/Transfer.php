<?php

/**
 * Class Transfer
 *
 * @property PDO $db
 * @property integer $sequence
 * @property integer $symbol
 * @property array $bufferRow
 * @property array $timeBar
 *
 */
class Transfer
{

    /** @var $db PDO */
    private $db;

    /** @var  integer $sequence ID of buffer row */
    private $sequence;

    /** @var integer $symbol ID of stock symbol */
    private $symbol;

    /** @var array $bufferRow Row from buffer table */
    private $bufferRow;

    /** @var array $timeBar Row from timeBar table */
    private $timeBar;

    /**
     * Magic getter
     *
     * @param $name
     * @return mixed
     * @throws Exception
     */
    public function __get($name)
    {
        if (method_exists($this, $method = ('get' . ucfirst($name))))
            return $this->$method();
        else
            throw new Exception('Can\'t get property ' . $name);
    }

    /**
     * Magic setter
     *
     * @param $name
     * @param $value
     * @return mixed
     * @throws Exception
     */
    public function __set($name, $value)
    {
        if (method_exists($this, $method = ('set' . ucfirst($name))))
            return $this->$method($value);
        else
            throw new Exception('Can\'t set property ' . $name);
    }

    /**
     * @return int
     */
    public function getSequence()
    {

        if (isset($this->sequence))
            return $this->sequence;

        /** @todo Move params in config */
        /** @var $query PDOStatement */
        $query = $this->db->prepare("SELECT MAX(id) AS id FROM sequence WHERE label = :label");
        $query->execute(array(
            ":label" => "buffer"
        ));


        /** @var $result array */
        $result = $query->fetch(PDO::FETCH_ASSOC);
        if ($result['id']) {
            /** @var integer $seq Sequence of current buffer id row */
            $this->sequence = $result['id'];
        } else {
            /** @todo Может стоит брать первый(младший) id из таблицы buffer */
            /** @var integer sequence */
            $this->sequence = 1;

            /** @var $query PDOStatement */
            $query = $this->db->prepare(
                "INSERT INTO " . App::$config['sequence']['tableName'] . " (`label`) values (:label)"
            );
            $query->execute(array(
                ":label" => "buffer",
            ));
        }

        return $this->sequence;
    }

    /**
     * @param int $sequence
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    }

    /**
     * @return PDO
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @param PDO $db
     * @throws Exception
     */
    public function setDb(PDO $db)
    {
        if ($db instanceof PDO)
            $this->db = $db;
        else
            throw new Exception("Try set not PDO");
    }

    /**
     * @return int
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @param int $symbol
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
    }

    /**
     * @return array
     */
    public function getPeriods()
    {
        return array(
            Symbol::MINUTE,
            Symbol::HOUR,
            Symbol::DAY
        );
    }


    /**
     * @param bool $refresh
     * @return array|mixed
     * @throws Exception
     */
    public function getBufferRow($refresh = true)
    {
        if ((isset($this->bufferRow)) && $refresh === false)
            return $this->bufferRow;

        /** @var $query PDOStatement */
        $query = $this->db->prepare("SELECT * FROM " . App::$config['buffer']['tableName'] . " WHERE id = :seq");
        $query->execute(array(
            ":seq" => $this->getSequence()
        ));

        if ($query->errorCode() != 0)
            throw new Exception($query->errorInfo());

        // sleep if result empty
        if ($query->rowCount() == 0) {
            usleep(App::$config['transfer']['sleep']);
            $this->getBufferRow();
        }

        /** @var $bufferRow array */
        $this->bufferRow = $query->fetch(PDO::FETCH_ASSOC);

        if(empty($this->bufferRow['symbol_id'])) {
            throw new Exception("Empty symbol_id. RowCount: ".$query->rowCount()." on seq:".$this->getSequence() .var_dump($this->bufferRow));
        }

        return $this->bufferRow;
    }

    /**
     * @param $period
     * @return bool|mixed
     */
    public function getTimeBar($period)
    {
        /** @var $table string */
        $table = Symbol::getTable($this->bufferRow['symbol_id'], $period);
        /** @var $query PDOStatement */
        $query = $this->db->prepare("SELECT * FROM " . $table . " WHERE datetime = :datetime");
        $query->execute(array(
            ":datetime" => date("Y-m-d H:i", strtotime($this->bufferRow['time'])),
        ));

        if ($query->rowCount() == 0)
            return false;
        else
            return $this->timeBar = $query->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param $period
     * @throws Exception
     * @return bool
     */
    public function createTimeBar($period)
    {
        /** @var $table string */
        $table = Symbol::getTable($this->bufferRow['symbol_id'], $period);
        /** @var $query PDOStatement */
        $query = $this->db->prepare(
            "INSERT INTO " . $table . " (`datetime`, `open`, `close`, `max`, `min`) values (:datetime, :open, :close, :max, :min)"
        );
        $query->execute(array(
            ":datetime" => date("Y-m-d H:i", strtotime($this->bufferRow['time'])),
            ":open" => $this->bufferRow['bid'],
            ":close" => $this->bufferRow['ask'],
            ":min" => $this->bufferRow['bid'],
            ":max" => $this->bufferRow['ask'],
        ));

        if ($query->errorCode() != 0)
            throw new Exception($query->errorCode() . " / " . $query->errorInfo());
        else
            return true;
    }


    /**
     * @param $period
     * @return bool
     * @throws Exception
     */
    public function updateTimeBar($period)
    {
        if (empty($this->timeBar) || empty($this->bufferRow))
            throw new Exception("TimeBar / BufferRow is empty");


        $tempRow = array();
        $tempRow['max'] = ($this->timeBar['max'] > $this->bufferRow['ask']) ? $this->timeBar['max'] : $this->bufferRow['ask'];
        $tempRow['min'] = ($this->timeBar['min'] < $this->bufferRow['bid']) ? $this->timeBar['min'] : $this->bufferRow['bid'];
        $tempRow['close'] = $this->bufferRow['bid'];

        /** @var $table string */
        $table = Symbol::getTable($this->bufferRow['symbol_id'], $period);
        /** @var $query PDOStatement */
        $query = $this->db->prepare(
            "UPDATE " . $table . " SET `close`=:close, `max`=:max, `min`=:min WHERE `datetime`=:datetime"
        );
        $query->execute(array(
            ":close" => $tempRow['close'],
            ":min" => $tempRow['min'],
            ":max" => $tempRow['max'],
            ":datetime" => $this->timeBar['datetime'],
        ));

        if ($query->errorCode() != 0)
            throw new Exception($query->errorCode() . " / " . $query->errorInfo());
        else
            return true;

    }


    /**
     * @return bool
     * @throws Exception
     */
    public function incrementSequence()
    {
        ++$this->sequence;

        /** @var $query PDOStatement */
        $query = $this->db->prepare(
            "INSERT INTO " . App::$config['sequence']['tableName'] . " (`label`) values (:label)"
        );
        $query->execute(array(
            ":label" => App::$config['buffer']['sequenceLabel'],
        ));

        if ($query->errorCode() != 0)
            throw new Exception($query->errorCode() . " / " . $query->errorInfo());
        else
            return true;
    }

}