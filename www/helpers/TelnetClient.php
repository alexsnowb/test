<?php
/**
 * Telnet client for aggregate data from telnet source
 */

class TelnetClient {
    /** @var  $host string */
    public $host;

    /** @var  $port integer */
    public $port;

    /** @var  $socket resource */
    public $socket;

    public function __construct($host, $port)
    {
        $this->host = $host;
        $this->port = $port;
    }

    /**
     *  Create a socket
     *
     * @return resource
     */
    public function socketCreate()
    {
        if(!($this->socket = socket_create(AF_INET, SOCK_STREAM, 0)))
        {
            $errorCode = socket_last_error();
            $errorMessage = socket_strerror($errorCode);

            die("Couldn't create socket: [$errorCode] $errorMessage \n");
        }

        return $this->socket;
    }

    /**
     * Connect to telnet
     *
     * @return bool
     */
    public function connectTelnet()
    {
        //Connect socket to remote server
        if(!($result = socket_connect($this->socket , $this->host , $this->port)))
        {
            $errorCode = socket_last_error();
            $errorMessage = socket_strerror($errorCode);

            die("Could not connect: [$errorCode] $errorMessage \n");
        }

        return $result;
    }

    public function getTicks()
    {
        if(!($result = socket_last_error($this->socket)))
        {
            if($buffer=socket_read($this->socket, 512, PHP_NORMAL_READ)){
                return $buffer;
            }
        }
    }
}